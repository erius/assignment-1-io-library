%define STDIN 0
%define STDOUT 1
%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60
%define EOF 4

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT ; set command to exit
    syscall ; call command

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ; put 0 in rax, will store string length in here
    .loop: ; loop start
    test byte [rdi+rax], 0xff ; see if char is 0 (NUL)
    je .end ; if NUL - stop
    inc rax ; else increase string length by 1
    jmp .loop ; continue checking
    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length ; calculate string's length
    pop rsi ; set start of the string
    mov rdx, rax ; set string length
    mov rdi, STDOUT ; set destination to stdout
    mov rax, SYS_WRITE ; set command to write
    syscall ; call command
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n` ; put 0xA (newline char) in rax
    ; using tail call optimization here to get rid of one of the calls to print_char

; Принимает код символа и выводит его в stdout
print_char:
    ; system write command requires the starting address of the string or a char in MEMORY
    ; that's why we are pushing the char to stack, so we could access it and then print it
    push rdi ; push char to stack (can't print from reg)
    mov rsi, rsp ; set start of the string
    mov rdx, 1 ; set string length to 1 (char length)
    mov rdi, STDOUT ; set destination to stdout
    mov rax, SYS_WRITE ; set command to write
    syscall ; call command
    pop rax ; restore stack to initial state
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi ; check whether the number is negative or positive
    jns print_uint ; if positive, simply do print_uint
    push rdi ; save the number to stack
    mov rdi, '-' ; load the "-" character to rdi
    call print_char ; print the "-" character
    pop rdi ; put the number back in rdi
    neg rdi ; since we already printed the "-", treat the number as an uint (by negating it) and print it after the "-"
    ; using tail call optimization here to get rid of one of the calls to print_uint

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp ; save calee-saved reg
    push 0 ; push 0 to stack so that we dont override bytes of already existing words
    mov rax, rdi ; moving number to rax
    mov rcx, 10 ; the divisor
    .loop: ; loop start
    xor rdx, rdx ; when using div, rdx will store the remainder
    ; it should always be 0 before division, or else it'll throw arithmetic exception
    div rcx ; divide number by 10, the digit's value is put into rdx
    add rdx, '0' ; add ascii code of 0 to get an ascii code of a digit
    dec rsp ; decrement stackpointer to continue writing number into the stack
    mov byte [rsp], dl ; dl is the lesser byte of rdx, this is where our digit's ascii code is stored
    ; move it to the byte that the rsp is pointing to
    test rax, rax ; check if we are out of digits (rax == 0)
    je .end ; if we are, exit the loop
    jmp .loop ; else continue
    .end:
    mov rdi, rsp ; move start of the string (rsp) to rdi (this is where the print_string fucntion looks for the argument)
    call print_string ; print the string
    mov rsp, r8 ; retreieve calee-saved reg
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax ; will be used as function output, put 0
    xor r8, r8 ; will be used as string offset, put 0
    .loop: ; loop start
    mov cl, [rsi+r8] ; get the char of the 1st string
    mov dl, [rdi+r8] ; get the char of the 2nd string
    cmp cl, dl ; compare
    jne .not_equal ; if not equal, reutrn 0
    test cl, cl ; check if strings ended
    je .equal ; if they did, increment result and return
    inc r8 ; increment string offset
    jmp .loop ; continue loop
    .equal:
    inc rax
    .not_equal:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0 ; push 0 to make space for a new char
    mov rsi, rsp ; set string start to stackpointer
    mov rdx, 1 ; set buffer size to 1 byte
    xor rdi, rdi ; set file to stdin (0)
    xor rax, rax ; set command to read (0)
    syscall ; call command
    pop rax ; get char
    cmp rax, EOF ; if rax is EOF, put 0 in rax, else OK
    jne .not_eof
    xor rax, rax
    .not_eof:
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    .loop_leading: ; start loop to find start of the word
    push rsi
    push rdi ; save caller-saved regs
    call read_char ; read the char
    pop rdi ; retrieve caller-saved regs
    pop rsi
    cmp al, `\t` ; if the char is one of the space chars (\t, \n, space) then continue the loop
    je .loop_leading
    cmp al, `\n`
    je .loop_leading
    cmp al, ' '
    je .loop_leading
    test al, al ; if the string has ended and the word wasn't found yet, goto fail
    je .fail
    xor r8, r8 ; put 0 into r8, will be used to store current char index
    .loop_main: ; start loop to find end of the word
    mov byte [rdi+r8], al ; save the char to buffer
    inc r8 ; increment char index
    cmp rsi, r8 ; check if the word exceeds the buffer size, if so, goto fail
    je .fail
    push rsi
    push rdi ; save caller-saved regs
    call read_char
    pop rdi ; retrieve caller-saved regs
    pop rsi
    cmp al, `\t` ; if the char is NOT one of the space chars (\t, \n, space) then stop the loop
    je .success
    cmp al, `\n`
    je .success
    cmp al, ' '
    je .success
    test al, al ; if the string has ended before the buffer size was exceeded, stop the loop
    je .success
    jmp .loop_main
    .success: ; when the word was successfully found and the buffer size was not exceeded
    mov byte [rdi+r8], 0 ; put NUL at the end of the word
    mov rax, rdi ; put buffer address into rax
    mov rdx, r8 ; put word length into rdx
    ret
    .fail: ; when the word wasn't successfully found or the buffer size was exceeded
    xor rax, rax ; put 0 into rax
    xor rdx, rdx ; put 0 into rdx
    ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-' ; check if the number starts with the "-"
    je .negative ; if its not, treat it as uint and goto parse_uint
    call parse_uint ; parse number as uint
    test rdx, rdx ; check if the number length is 0, if so goto fail
    je .fail
    test rax, rax ; test rax to check its sign
    js .fail ; check if the number is really positive, if it is not, goto fail
    ; this could happen if the number exceeds signed int limits, while being a valid unsigned int
    ; i.e. if we work with 8-bit number, 255 will be 255 as uint, but when we treat it as a signed int, we get -1, which is wrong
    ret
    .negative:
    inc rdi ; else increment rdi (so that the buffer pointer points to number and not the "-")
    call parse_uint ; treat the part after the "-" as uint and call parse_uint
    dec rdi ; decrement rdi so it remains the same after this function (rdi is callee-saved reg)
    test rdx, rdx ; check if the number length is 0, if so goto fail
    je .fail
    neg rax ; negate the resulting number, changing it from uint back to int
    jns .fail ; check if the number is really negative, same thing as on line 193
    inc rdx ; increment number length, since we need to account for the "-" too
    ret
    .fail:
    xor rax, rax ; put 0 into rax
    xor rdx, rdx ; put 0 into rdx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax ; put 0 in rax, will be used to store uint
    mov r8, 10 ; put 10 in r8, will be used in muls
    xor r9, r9 ; put 0 in r9, will be used as current char index
    .loop: ; start loop
    xor rcx, rcx ; put 0 in rcx so it won't have any numbers from previous calculations left
    mov cl, [rdi+r9] ; put the char into the lesser byte of rcx (cl)
    test cl, cl ; if string ended, goto stop
    je .stop
    cmp cl, '0' ; if the char isnt in range ["0"-"9"], goto stop
    jb .stop
    cmp cl, '9'
    ja .stop
    sub cl, '0' ; subtract "0" from char to get digit
    mul r8 ; multiply rax by 10 to make space for a new digit
    jc .fail ; carry 1 during mul means that the unsigned number is too big and cant be parsed, goto fail
    add rax, rcx ; add digit to rax
    inc r9 ; increase char index
    jmp .loop ; continue loop
    .stop: ; when the string has ended or we've hit a non-numeric char
    test r9, r9 ; check if the numbers length is 0
    je .fail ; if it is 0, that means that there was no number, goto fail
    mov rdx, r9 ; move number length to rdx
    ret
    .fail: ; when the number is too big or the length is 0
    xor rax, rax ; put 0 into rax
    xor rdx, rdx ; put 0 into rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера (rdi, rsi, rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi ; save caller-saved regs
    push rsi
    push rdx
    call string_length ; get length of string
    pop rdx ; retrieve caller-saved regs
    pop rsi
    pop rdi
    cmp rax, rdx ; if string length exceeds the buffer size, goto fail
    ja .fail
    xor rdx, rdx ; put 0 in rdx, will be used to store char index
    xor rcx, rcx ; put 0 in rcx, will be used to temporarily store the char when moving from string to buffer (mem to mem mov is impossible)
    .loop:
    mov cl, byte [rdi+rdx] ; move char from the string to lesser byte of rcx (cl)
    mov byte [rsi+rdx], cl ; move same char into buffer
    test cl, cl ; if char is 0 (NUL), string successfully copied, goto success
    je .success
    inc rdx ; increment char index
    jmp .loop ; continue the loop
    .fail: ; when the string is too big for the buffer
    xor rax, rax ; put 0 into rax
    .success: ; when the string was successfully copied to the buffer
    ret
